<?php
/**
 * Created by PhpStorm.
 * User: dimple
 * Date: 28.03.19
 * Time: 16:01
 */

namespace Fastcms\Model;


use Fastcms\App\Model;

class Page extends Model
{

    public function getPages($offset = 0,$limit = 10){
       return $this->db->query("select * from ". DB_PREFIX. "pages ORDER BY id limit :limit offset :offset", [":limit" => $limit, ":offset" => $offset]);
    }

    public function getTotalCountPages(){
       $count = $this->db->query("select count(*) as count from ". DB_PREFIX ."pages");
       return $count[0]['count'];
    }

    public function getPageById($id){
        //$id = addslashes($id);
        return $this->db->query("select * from ". DB_PREFIX ."pages where id=:id",[":id" => $id]);
    }

    public function addPage($seo_url, $name, $priority = 0.8, $language_id = 1, $title = "", $description = "", $creator_id = "1"){
        $this->db->query("INSERT INTO ". DB_PREFIX ."pages (seo_url, name, priority, language_id, meta_title, meta_description, date_add, creator_id) 
            VALUES (:seo_url, :name, :priority, :language_id, :title, :description, CURRENT_TIMESTAMP,:creator_id);" ,
            [":seo_url" => $seo_url, ":name" => $name, ":priority" => $priority, ":language_id" => $language_id, ":title" => $title, ":description" => $description, ":creator_id" => $creator_id]);
    }

    public function deletePage($id){
        $this->db->query("delete from ". DB_PREFIX ."pages where id=:id",[':id' => $id]);
    }

    public function savePage($param){
        $this->db->query("UPDATE ". DB_PREFIX ."pages SET name=:name, seo_url=:seo_url,priority=:priority,language_id=:language_id,meta_title=:title,meta_description=:description WHERE id=:id",$param);
    }

    public function addBlock($page_id,$block_id,$sort_pos,$typeBlock){
        $this->db->query("INSERT INTO ". DB_PREFIX ."page_block(id_page,id_block,sort_pos,type) values(:id_page,:id_block,:sort_pos,:type)",[":id_page" => $page_id, ":id_block" => $block_id, ":sort_pos" => $sort_pos,":type" => $typeBlock]);
    }

    public function removeBlock($page_id,$block_id){
        $this->db->query("DELETE FROM ". DB_PREFIX ."page_block where id_page=:id_page and id_block=:id_block",[":id_page" => $page_id, ":id_block" => $block_id]);
    }
}