<?php


namespace Fastcms\Controller;


use Fastcms\App\Controller;
use Fastcms\App\Router;
use Fastcms\App\Session;
use Fastcms\Model\Posts;

class PostsController extends Controller
{

    public function index(){
        if(!Session::get('user')){
            $data['close'] = "You haven't got";
            $this->render('posts/posts',["data" => $data]);
        } else {

            $model = new Posts();

            //$countPages = $model->getTotalCountPages();
            //$currentPage = isset($_GET['page']) ? $_GET['page'] : 1;
            //$pagination = new PaginationWidget(['itemsCount' => $countPages, 'itemsPerPage' => 10,'currentPage' => $currentPage ]);
            //$pages = $model->getPages($pagination->offset,$pagination->limit);
            $post_blocks = $model->getPostBlocks();
            $this->render('posts/posts',['postBlocks' => $post_blocks, "title" => "Посты"]);
        }
    }

    public function createBlock(){
        $model = new Posts();
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (!empty($_POST['name'])) {
                $name = $_POST['name'];
                $type = "posts_block";
                $model->addBlockPost($name, $type);
                Router::redirect("/admin/posts");
            }
        }
        $this->render("/posts/create-block",["title" => "Создание блока постов"]);
    }

    public function createPost(){
        $model = new Posts();
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (!empty($_POST['title'])) {
                $id_block = $_POST['id_block'];
                $param = [
                    "id_block" => $_POST['id_block'],
                    "title" => $_POST['title'],
                    "seo_url" => $_POST['seo_url'],
                    "priority" => $_POST['priority'],
                    "meta_title" => $_POST['meta_title'],
                    "meta_description" => $_POST['meta_description'],
                    "link" => $_POST['link'],
                    "preview_text" => $_POST['preview_text'],
                    "full_text" => $_POST['full_text'],
                    "tags" => $_POST['tags'],
                ];
                $model->createPost($param);
                Router::redirect("/admin/posts/view/".$id_block);
            }
        } else {
            $id_block = $this->params[0];
            $this->render("/posts/create-post",["id_block" => $id_block,"title" => "Создание поста"]);
        }
    }

    public function edit(){
        $model = new Posts();
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (!empty($_POST['title'])) {
                $id_post = $_POST['id_post'];
                $param = [
                    "id" => $id_post,
                    "title" => $_POST['title'],
                    "seo_url" => $_POST['seo_url'],
                    "priority" => $_POST['priority'],
                    "meta_title" => $_POST['meta_title'],
                    "meta_description" => $_POST['meta_description'],
                    "link" => $_POST['link'],
                    "preview_text" => $_POST['preview_text'],
                    "full_text" => $_POST['full_text'],
                    "tags" => $_POST['tags'],
                ];
                $model->updatePost($param);
                Router::redirect("/admin/posts/edit/".$id_post);
            }
        } else {
            $id_post = $this->params[0];

            $post = $model->getPost($id_post);

            $this->render("/posts/edit-post",["post" => $post,["title" => "Редактирование поста"]]);
        }
    }

    public function view(){
       // $id = $_GET['id'];

        $id = $this->params[0];

        $model = new Posts();

        $posts = $model->getPosts($id);
        $block = $model->getBlock($id);
        $this->render('posts/view',['posts' => $posts,"block" => $block[0],"title" => "Посты {$block[0]['name']}"]);
    }

    public function delete(){
        $id = $_GET['id'];
        $idBlock = $_GET['id_block'];

        $model = new Posts();
        $model->deletePost($id);
        Router::redirect("/admin/posts/view/".$idBlock);
    }

    public function deleteBlock(){
        $id = $_GET['id'];

        $model = new Posts();
        $model->deleteBlock($id);
        Router::redirect("/admin/posts");
    }

}