
<header>
    <h1>Панель управления</h1>
    <div class="breadcrumbs">
        <a href="/admin">Панель управления</a>
        <a href="/admin/sliders">Слайдеры</a>
        <a href="/admin/sliders/create-block">Создание блока слайдера</a>
    </div>
</header>

<form action="/admin/slider/create-block" method="POST">
    <input type="text" name="name">
    <input type="submit" value="Add block slider">
</form>