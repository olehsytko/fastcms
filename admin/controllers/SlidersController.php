<?php


namespace Fastcms\Controller;


use Fastcms\App\Controller;
use Fastcms\App\Router;
use Fastcms\App\Session;
use Fastcms\Model\Slider;

class SlidersController extends Controller
{

    public function index(){
        $model = new Slider();

        $sliders = $model->getSliders();

        $this->render('slider/sliders', ["sliders" => $sliders, "title" => "Слайдеры"]);
    }

    public function view(){
        $id = $this->params[0];

        $model = new Slider();

        $slider = $model->getSlider($id);
        $block = $model->getBlock($id);

        $this->render('slider/view', ['slider' => $slider,'block' => $block, "title" => "Слайдер {$block['name']}"]);
    }

    public function createBlock(){
        $model = new Slider();
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (!empty($_POST['name'])) {
                $name = $_POST['name'];
                $type = "slider_block";
                $model->addBlockSlider($name, $type);
                Router::redirect("/admin/sliders");
            }
        }
        $this->render("slider/create-block", ["title" => "Создание слайдера"]);
    }

}