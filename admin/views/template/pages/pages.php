<?php if(isset($data['close'])){
    echo $data['close'];
} else {
?>

    <header>
        <h1>Панель управления</h1>
        <div class="breadcrumbs">
            <a href="/admin">Панель управления</a>
            <a href="/admin/pages">Страницы</a>
        </div>
    </header>

    <div class="language">
    <span>Показаны страницы для языка: </span>
    <div class="select">
        <div class="selected">
            <span>Русский</span>
            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24">
                <path fill="none" d="M0 0h24v24H0V0z" />
                <path d="M7.41 8.59L12 13.17l4.59-4.58L18 10l-6 6-6-6 1.41-1.41z" />
            </svg>
        </div>
        <div class="list">
            <span>Русский</span>
            <span>English</span>
            <span>Українська</span>
        </div>
    </div>
    </div>

    <a href="/admin/pages/create">Create page</a>

<?php foreach ($pages as $page):?>
        <div class="card">
            <span><?=$page['name']?></span>
            <span><?=$page['seo_url']?></span>
            <div class="btn-container">
                <a href="/admin/pages/edit/<?=$page['id']?>" title="Редактировать" class="blue">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path fill="none" d="M0 0h24v24H0V0z" />
                        <path
                                d="M14.06 9.02l.92.92L5.92 19H5v-.92l9.06-9.06M17.66 3c-.25 0-.51.1-.7.29l-1.83 1.83 3.75 3.75 1.83-1.83c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.2-.2-.45-.29-.71-.29zm-3.6 3.19L3 17.25V21h3.75L17.81 9.94l-3.75-3.75z" />
                    </svg>
                </a>
                <a href="/admin/pages/delete?id=<?=$page['id']?>" title="Удалить" class="red" data-delete>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path fill="none" d="M0 0h24v24H0V0z" />
                        <path d="M16 9v10H8V9h8m-1.5-6h-5l-1 1H5v2h14V4h-3.5l-1-1zM18 7H6v12c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7z" />
                    </svg>
                </a>
            </div>
        </div>
<?php endforeach; ?>

    <?php foreach ($p->buttons as $button) :
        if ($button->isActive) : ?>
            <a href = '?page=<?=$button->page?>'><?=$button->text?></a>
        <?php else : ?>
            <span style="color:#555555"><?=$button->text?></span>
        <?php endif;
    endforeach; ?>

<?php } ?>