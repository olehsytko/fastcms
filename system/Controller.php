<?php 

namespace Fastcms\App;

abstract class Controller{

	protected $data;
	protected $params;
    protected $get_params;
	protected $path;

	public function __construct($data = array()){
		$this->data = $data;
		$this->params = App::getRouter()->getParams();
        $this->get_params = App::getRouter()->getParamsGet();
	}


	public function getData(){
		return $this->data;
	}

	public function getModel(){
		return $this->model;
	}

	public function getParams(){
		return $this->params;
	}

    public function getParamsGet(){
        return $this->get_params;
    }

	public function render($view, $compact = array()){

       // foreach ($compact as $kay => $arr){
         //   ${$kay} = $arr;
        //}
        extract($compact);

        $layout_path = VIEWS_PATH.DS.$view.'.php';

        if(isset($this->layout)){
            $layout = $this->layout;
        } else {
            $layout = 'layout';
        }

        $layout_main = VIEWS_PATH.DS.'layout'.DS.$layout.'.php';

        ob_start();
        include $layout_path;
        $content = ob_get_clean();

        $data['content'] = $content;

        ob_start();
        include $layout_main;
        $view = ob_get_clean();

        echo $view;
    }

}