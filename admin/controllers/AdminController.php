<?php
/**
 * Created by PhpStorm.
 * User: dimple
 * Date: 26.03.19
 * Time: 12:36
 */

namespace Fastcms\Controller;

use Fastcms\App\Controller;
use  Fastcms\Model\Admin;
use  Fastcms\Model\User;

class AdminController extends Controller{

    public function index(){
       // $this->data['content'] = "Admin Controller, action index ";
        //$this->data['title']  = "Index";

        $this->render('admin');
    }
}