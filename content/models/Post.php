<?php

namespace Fastcms\Site\Model;


use Fastcms\App\Model;

class Post extends Model
{
    public function getPostBlock($idBlock,$offset,$limit){
      return $this->db->query("SELECT * FROM ". DB_PREFIX ."post where id_post_block=:id_block ORDER BY id limit :limit offset :offset",[":id_block" => $idBlock,":limit" => $limit, ":offset" => $offset]);
    }

    public function getTotalCountPosts($idBlock){
        return $this->db->query("SELECT count(*) as count FROM ". DB_PREFIX ."post where id_post_block=:id_block", [":id_block" => $idBlock])[0]['count'];
    }

    public function getPost($idPage,$postUrl){
        return $this->db->query("SELECT ps.title as title,ps.seo_url as seo_url,ps.full_text as full_text,ps.meta_title as meta_title,ps.meta_description,ps.tags as tags,ps.img as img 
                                FROM fc_post as ps INNER join fc_page_block as pb ON(ps.id_post_block=pb.id_block) inner join fc_pages as p ON(p.id=pb.id_page)
                                where p.id=:id_page and ps.seo_url like :post_url", [":id_page" => $idPage, ":post_url" => $postUrl]);
    }
}