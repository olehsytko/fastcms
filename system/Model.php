<?php

namespace Fastcms\App;

abstract class Model{

	protected $db;

	public function __construct(){
		$this->db = App::$db;
	} 
}