<?php

namespace Fastcms\App;

use Fastcms\App\Config;
use Fastcms\App\db;
use Fastcms\App\Router;
use Fastcms\App\Session;
use Fastcms\Site\Controller\PagesController;

class App{

    protected static $router;

    public static $db;

    private function __clone() {}
    private function __construct() {}
    private function __sleep(){}
    private function __wakeup(){}

    public static function getRouter(){
        return self::$router;
    }

    public static function startAdminPanel($uri){

        $default['routes'] = Config::get('routes');
        $default['route'] = Config::get('defaultRouteAdminPanel');
        $default['language'] = Config::get('defaultLanguageAdminPanel');
        $default['controller'] = Config::get('defaultControllerAdminPanel');
        $default['action'] = Config::get('defaultActionAdminPanel');

        self::$router = new Router($uri, $default);

        self::$db = new DB(Config::get('db.host'),Config::get('db.user'),Config::get('db.password'),Config::get('db.db_name'));

        $controller_class = ucfirst(self::$router->getController()).'Controller';
        $controller_method = strtolower(self::$router->getMethodPrefix().self::$router->getAdminAction());

        $layout = self::$router->getRoute();
        if(empty(Session::get('user')) && $controller_method != "login"){
//            if( !empty($_COOKIE['login']) and !empty($_COOKIE['id']) ) {
//                Session::set('user', 'login');
//                Session::set('id', $_COOKIE['id']);
//                Session::set('login', $_COOKIE['login']);
//            }
            Router::redirect("/admin/user/login");
        }

        //Calling controller's method
        $controller = "Fastcms\\Controller\\".$controller_class;
        $controller_object = new $controller();
        $controller_object->$controller_method();
    }

    public static function startSite($uri){
        $default['routes'] = Config::get('routes');
        $default['route'] = Config::get('defaultRouteSiteContent');
        $default['language'] = Config::get('defaultLanguageSiteContent');
        $default['controller'] = Config::get('defaultControllerSiteContent');
        $default['action'] = Config::get('defaultActionSiteContent');

        self::$router = new Router($uri,$default);

        self::$db = new DB(Config::get('db.host'),Config::get('db.user'),Config::get('db.password'),Config::get('db.db_name'));

        $sitePage = self::$router->getController();
        $pageSection = strtolower(self::$router->getSiteAction());

       // echo  self::$router->getLanguage();

        $page = new PagesController();
        $page->viewPage($sitePage, $pageSection);
    }
}