<?php

namespace Fastcms\App;

class DB{

	protected $db;

	public function __construct($host, $user, $password, $db_name){
	    try {
	        $this->db = new \PDO("mysql:host={$host};dbname={$db_name}", $user, $password);
	        $this->db->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
        } catch (\PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
	    }
	}

	public function query($sql,$params = []){
		if( !$this->db ){
			return false;
		}

        $data = array();

        try {
            $stmt = $this->db->prepare($sql);
            $stmt->execute($params);

            if( is_bool($stmt) ){
                return $stmt;
            }

            while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                $data[] = $row;
            }
            $stmt = null;
		} catch (\PDOException $e) {
            print $e->getMessage();
        }

		return $data;
	}
}
