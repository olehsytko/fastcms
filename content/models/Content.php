<?php


namespace Fastcms\Site\Model;


use Fastcms\App\Model;

class Content extends Model
{
    public function getContent($idBlock){
        return $this->db->query("SELECT text FROM ". DB_PREFIX ."content as c WHERE id_block=:id_block", [":id_block" => $idBlock])[0];
    }

}