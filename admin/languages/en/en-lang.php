<?php 

$lang_text['title'] = "Admin Panel";
$lang_text['change'] = "Edit";
$lang_text['del'] = "Delete";
$lang_text['save'] = "Save";
$lang_text['cancel'] = "Cancel";
$lang_text['text3'] = "Are you sure you want to delete ";
$lang_text['text4'] = "? You will not be able to cancel this action!";

$lang_text['nav_main'] = 'Main';
$lang_text['nav_pages'] = 'Pages';
$lang_text['nav_main-parts'] = 'Main parts';
$lang_text['nav_posts'] = 'Posts';
$lang_text['nav_content'] = 'Content';
$lang_text['nav_settings'] = 'Settings';
$lang_text['nav_users'] = 'Users';


$lang_text['wlcome'] = "Welcome to the admin panel";
$lang_text['main_text1'] = "Aim at the section on the left to view detailed information."; 
$lang_text['main_text2'] = "To quickly save changes, you can press the key combination Ctrl + S.";
$lang_text['main_text3'] = "Database successfully connected";
$lang_text['main_text8'] = "Database is not connected";
$lang_text['main_text4'] = "The site is closed from the index!";
$lang_text['main_text9'] = "Site is indexed";
$lang_text['main_text5'] = "Multilingual included!";
$lang_text['main_text10'] = "Multi language disabled";
$lang_text['main_text6'] = "Connection with css and js files";
$lang_text['main_text7'] = "Observe the structure and name of folders and files!";

$lang_text['main_text11'] = "This section contains all the pages of the site with their contents.";
$lang_text['main_text12'] = 'Creating blocks "Displaying posts" and "Displaying slider" automatically creates blocks in the appropriate sections. Blocks can be swapped by using the ';
$lang_text['main_text13'] = 'For each language you need to create a separate page. You can create a language in the "Settings" section. ';
$lang_text['main_text14'] = "Create clear and logical names for the blocks, do not confuse yourself and the developers!";
$lang_text['main_text15'] = 'The main page in the "Alias" field should have the word "index".';       
$lang_text['main_text16'] = "This section contains the top (header.php) and bottom (footer.php) parts of the site.";
$lang_text['main_text17'] = "For the top of the main page, use the second editor (mainPage-header.php).";
$lang_text['main_text18'] = 'In this section you can add any posts / news to the pages. They will be displayed in the "Posts" block. The block "Posts" can be added to the site pages anywhere. To do this, go to the "Pages" section, select the page of interest to you, then click edit. In the list of blocks you can find a block of already created records or add a new block. To add a new block, click "Add block", then select the "Record output" block, enter the name of the Post and click "Save". The entry will automatically be created in the "Posts" section, where it can be filled or modified. Pagination for each post is configured separately. ';
$lang_text['main_text19'] = "- Before starting the post / news insert the word BEGIN, after the post / news insert the word END, replace the name with the word HEADER, replace the image path with the word IMG, you can use FULL_TEXT, LINK, INPUT_1 in the same way, INPUT_2, INPUT_3. ";
$lang_text['main_text20'] = 'In this section you can edit sliders. To create a slider, go to the "Pages" section, select a page and add a new block. ';
$lang_text['main_text21'] = "In the name field, enter the name of the block";
$lang_text['main_text22'] = "Turn on developer mode in the editor, to do this, click on the";
$lang_text['main_text23'] = "and insert the slider code (The code should contain the slider structure and the slide template)";
$lang_text['main_text24'] = "- Insert the word BEGIN before the start of the slide, insert the word END after the slide, replace the name with the word NAME, replace the image path with the word IMG, you can use TEXT, DATE in the same way.";
$lang_text['main_text25'] = '- In the list select "Slider output".';
$lang_text['main_text26'] = "In this section, you can:";
$lang_text['main_text27'] = "Mount Database";
$lang_text['main_text28'] = "Paste onto the Google Tag Manager site";
$lang_text['main_text29'] = "Download favicon";
$lang_text['main_text30'] = "Set up multilingual";
$lang_text['main_text31'] = "Close / open site from index";
$lang_text['main_text32'] = "Set up redirects";
$lang_text['main_text33'] = "Specify the mail where the letters will come from the site (Copy the code of the sender of letters)";
$lang_text['main_text34'] = "In this section, you can add new access to the admin panel or change the already created ones.";
$lang_text['main_text35'] = "It is possible to create access for the editor (Blocked access to development sections).";
$lang_text['main_text36'] = "In this section, you can edit the css and js files that were uploaded to the appropriate folders in the site root.";
$lang_text['main_text37'] = "In this section, you can add new functionality to the admin panel.";

$lang_text['pages_text1'] = "Pages";
$lang_text['pages_text2'] = "Show";
$lang_text['pages_namepage'] = "Page title";

$lang_text['edit_page_text1'] = "Edit page";
$lang_text['edit_page_text2'] = "Go";
$lang_text['edit_page_text3'] = "Rename";
$lang_text['edit_page_text4'] = "Select block type";
$lang_text['edit_page_text5'] = "Displaying posts";
$lang_text['edit_page_text6'] = "Text block";
$lang_text['edit_page_text7'] = "Add";
$lang_text['edit_page_text8'] = "Title";

$lang_text['parts_text1'] = "Main parts";
$lang_text['parts_text2'] = "You are not authorized to view this page!";

$lang_text['edit_note_text1'] = "Edit page";
$lang_text['edit_note_text2'] = "Editing posts";
$lang_text['edit_note_text3'] = "Title";
$lang_text['edit_note_text4'] = "Alias";
$lang_text['edit_note_text5'] = "Photo preview";
$lang_text['edit_note_text6'] = "Select photo";
$lang_text['edit_note_text7'] = "Preview text";
$lang_text['edit_note_text8'] = "Full Text";

$lang_text['edit_rewiew_text1'] = "Edit content";
$lang_text['edit_rewiew_text2'] = "Editing content";
$lang_text['edit_rewiew_text3'] = "Title";
$lang_text['edit_rewiew_text4'] = "Photo preview";
$lang_text['edit_rewiew_text5'] = "Select Photo";
$lang_text['edit_rewiew_text6'] = "Name";
$lang_text['edit_rewiew_text7'] = "Photo";
$lang_text['edit_rewiew_text8'] = "Text";

$lang_text['settings_text1'] = "Settings";
$lang_text['settings_text2'] = "Code in <head>";
$lang_text['settings_text3'] = "Code after <body>";
$lang_text['settings_text4'] = "Connect to database";
$lang_text['settings_text5'] = "Host";
$lang_text['settings_text6'] = "Login";
$lang_text['settings_text7'] = "Password";
$lang_text['settings_text8'] = "Base name";
$lang_text['settings_text9'] = "Download favicon";
$lang_text['settings_text10'] = "Site Languages";
$lang_text['settings_text11'] = "Multilingual";
$lang_text['settings_text12'] = "Title";
$lang_text['settings_text13'] = "Alias";
$lang_text['settings_text14'] = "Close the site from robots";
$lang_text['settings_text15'] = "Sending letters to the mail";
$lang_text['settings_text16'] = "Title";
$lang_text['settings_text17'] = "From";
$lang_text['settings_text18'] = "Where";
$lang_text['settings_text19'] = "Admin Language";
$lang_text['settings_text20'] = "You are not authorized to view this page!";
$lang_text['settings_text21'] = "Default site language";


$lang_text['users_text1'] = "Login";
$lang_text['users_text2'] = "Password";
$lang_text['users_text3'] = "Editor";
$lang_text['users_text4'] = "You are not authorized to view this page!";