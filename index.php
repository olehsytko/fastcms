<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

define("DS", DIRECTORY_SEPARATOR);
define("ROOT", dirname(__FILE__));
define("VIEWS_PATH_VIEW", '/content'.DS.'views');
define("VIEWS_PATH", ROOT.DS.'content'.DS.'views'.DS.'template'.DS.'pages');
define("VIEWS_PATH_BASE", ROOT.DS.'content'.DS.'views'.DS.'template'.DS.'base');

//require_once(ROOT.DS."conf".DS."config.php");

require_once __DIR__ . '/system/vendor/autoload.php';

use Fastcms\App\App;

session_start();

App::startSite($_SERVER['REQUEST_URI']);