<?php

use Fastcms\App\Config;

//Config::set('site_name', 'Pomadoro Timer');

define('DB_PREFIX', 'fc_');

Config::set('languages', array('en','ru'));

// Routes. Route name => method prefix

Config::set('routes',array(
	'default' => '',
));

Config::set('defaultRouteAdminPanel', 'default');
Config::set('defaultLanguageAdminPanel', 'ru');
Config::set('defaultControllerAdminPanel', 'admin');
Config::set('defaultActionAdminPanel', 'index');

Config::set('defaultRouteSiteContent', 'default');
Config::set('defaultLanguageSiteContent', 'ru');
Config::set('defaultControllerSiteContent', 'index');
Config::set('defaultActionSiteContent', 'none');

Config::set('db.host', 'localhost');
Config::set('db.user', 'root');
Config::set('db.password', '233685614');
Config::set('db.db_name', 'fastcms');

Config::set('salt', 'gi84kdps95mg31hud1cnkg');