<?php if(isset($data['close'])):?>
    <?=$data['close'];?>
<?php else: ?>

    <header>
        <h1>Панель управления</h1>
        <div class="breadcrumbs">
            <a href="/admin">Панель управления</a>
            <a href="/admin/pages">Контент</a>
        </div>
    </header>

    <a href="/admin/content/create-block">Create a content block</a>

    <?php
    foreach ($contentBlocks as $contentBlock):?>
        <div class="card">
            <span><?=$contentBlock['name']?></span>
            <span><?=$contentBlock['date_add']?></span>
            <div class="btn-container">
                <a href="/admin/content/edit/<?=$contentBlock['id']?>" title="Редактировать" class="blue">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path fill="none" d="M0 0h24v24H0V0z" />
                        <path
                                d="M14.06 9.02l.92.92L5.92 19H5v-.92l9.06-9.06M17.66 3c-.25 0-.51.1-.7.29l-1.83 1.83 3.75 3.75 1.83-1.83c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.2-.2-.45-.29-.71-.29zm-3.6 3.19L3 17.25V21h3.75L17.81 9.94l-3.75-3.75z" />
                    </svg>
                </a>
                <a href="/admin/content/delete?id=<?=$contentBlock['id']?>" title="Удалить" class="red" data-delete>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path fill="none" d="M0 0h24v24H0V0z" />
                        <path d="M16 9v10H8V9h8m-1.5-6h-5l-1 1H5v2h14V4h-3.5l-1-1zM18 7H6v12c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7z" />
                    </svg>
                </a>
            </div>
        </div>
    <?php endforeach; ?>

<?php endif; ?>