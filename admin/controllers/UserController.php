<?php

namespace Fastcms\Controller;

use Fastcms\App\Config;
use Fastcms\App\Controller;
use Fastcms\App\Router;
use Fastcms\App\Session;
use Fastcms\Model\User;

class UserController extends Controller{

	public function __construct($data = array()){
		parent::__construct($data);
	}

	public function login(){

	    $model = new User();
		$this->data['error'] = '';
		if( $_POST && isset($_POST['login']) && isset($_POST['password']) ){
			$user = $model->getByLogin($_POST['login']);
			$hash = md5(Config::get('salt').$_POST['password']);
			if(!empty($user) && $hash == $user['pass']){
				Session::set('login', $user['login']);
				Session::set('id', $user['id']);
				Session::set('user', $user['login']);

//				setcookie('login', $user['login'], time()+60*60*24*7,'/'); //логин
//				setcookie('id', $user['id'], time()+60*60*24*7, '/');

				Router::redirect("/admin/main");
			} else {
				$this->data['error'] = 'Incorrect login or password';
			}
		}
        $this->render('login');
	}

    public function logout(){
	    Session::destroy();
//
//	    setcookie('login', "", time()-60*60*24*7,'/'); //логин
//        setcookie('id', "", time()-60*60*24*7, '/');

        Router::redirect("/admin/user/login");
    }

}