<?php


namespace Fastcms\Controller;


use Fastcms\App\Controller;
use Fastcms\App\Router;
use Fastcms\App\Session;
use Fastcms\Model\Content;

class ContentController extends Controller
{

    public function index(){
        if(!Session::get('user')){
            $data['close'] = "You haven't got";
            $this->render('content/content',["data" => $data]);
        } else {
            $modelContent = new Content();
            $contentBlocks = $modelContent->getContentBlocks();

            $this->render("content/content",["contentBlocks" => $contentBlocks, "title" => "Контент"]);
        }
    }

    public function createBlock(){
        if ($_SERVER["REQUEST_METHOD"] == "POST"){
            $modelContent = new Content();
            $param = [
                "name" => $_POST['name'],
                "text" => $_POST['content'],
            ];
            $param["type"] = "content_block";
            $modelContent->addContent($param);
            Router::redirect("/admin/content");
        } else {
            $this->render("content/create-block", ["title" => "Создание блока контента"]);
        }
    }

    public function delete(){
        $modelContent = new Content();
        $id = $_GET['id'];
        $modelContent->deleteContentBlock($id);
        Router::redirect("/admin/content");
    }

    public function edit(){
        if ($_SERVER["REQUEST_METHOD"] == "POST"){
            $modelContent = new Content();
            $param = [
                "name" => $_POST['name'],
                "text" => $_POST['content'],
                "id_content" => $_POST['id_content'],
                "id_block" => $_POST['id_block']
            ];
            $modelContent->updateContentBlock($param);
            Router::redirect("/admin/content");
        } else {
            $id_block = $this->params[0];

            $modelContent = new Content();

            $contentBlock = $modelContent->getContentBlock($id_block);

            $this->render("content/edit-content", ['contentBlock' => $contentBlock[0], "title" => "Редактирование контента"]);
        }
    }

}