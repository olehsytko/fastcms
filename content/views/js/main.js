<script src="system/js/jquery-3.2.1.min.js"></script>
    <script src="system/codemirror-5.43.0/lib/codemirror.js"></script>
    <script src="system/codemirror-5.43.0/mode/javascript/javascript.js"></script>
    <script src="system/js/main.js"></script>
    <script>
        var headerTextarea = CodeMirror.fromTextArea(document.getElementById('headerTextarea'), {
            lineNumbers: true,
            matchBrackets: true,
            mode: 'javascript',
            indentUnit: 3,
            scrollbarStyle: null,
            theme: 'neo',
            spellcheck: true
        });
        var headerTextarea = CodeMirror.fromTextArea(document.getElementById('header2Textarea'), {
            lineNumbers: true,
            matchBrackets: true,
            mode: 'javascript',
            indentUnit: 3,
            scrollbarStyle: null,
            theme: 'neo',
            spellcheck: true
        });
        var headerTextarea = CodeMirror.fromTextArea(document.getElementById('footerTextarea'), {
            lineNumbers: true,
            matchBrackets: true,
            mode: 'javascript',
            indentUnit: 3,
            scrollbarStyle: null,
            theme: 'neo',
            spellcheck: true
        });
    </script>
    <script>
        function language_change(){
            var lang = $('#lang_select').val();
            //alert(lang);
            var url = "/admin/parts?lang="+lang;
            window.location.replace(url);
        }
    </script>