<header>
    <h1>Панель управления</h1>
    <div class="breadcrumbs">
        <a href="/admin">Панель управления</a>
        <a href="/admin/content">Контент</a>
        <span>Редактирование контент блока</span>
    </div>
</header>

<form action="" method="post">
    <input type="hidden" value="<?=$contentBlock['id_content']?>" name="id_content">
    <input type="hidden" value="<?=$contentBlock['id_block']?>" name="id_block">
    Name
    <input type="text" name="name" value="<?=$contentBlock['name']?>">
    <br>
    <br>
    Input-1
    <input type="text" name="f-input-1" <?=$contentBlock['inputs']?>>
    <br>
    <br>
    Content
    <textarea name="content" id="edit1">
        <?=$contentBlock['text']?>
    </textarea>
    <br>
    <input type="submit" value="Update">
</form>

<script src="/system/widgets/ckeditor4/ckeditor.js"></script>
<!--<script src="https://cdn.ckeditor.com/ckeditor5/12.1.0/classic/ckeditor.js"></script>-->
<script>
    CKEDITOR.replace( 'content',{
        filebrowserBrowseUrl: '/system/widgets/ckfinder/ckfinder.html',
        filebrowserUploadUrl: '/system/widgets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
    });
</script>