<?php


namespace Fastcms\Model;


use Fastcms\App\Model;

class Posts extends Model
{

    public function addBlockPost($name, $type = "", $quantity = 0){
       return $this->db->query("INSERT INTO ". DB_PREFIX ."blocks (name, type, quantity, date_add) 
            VALUES (:name, :type, :quantity, NOW());" ,
            [":name" => $name, ":type" => $type, ":quantity" => $quantity]);
    }

    public function getPostBlocks($offset = 0,$limit = 10){
        return $this->db->query("select * from ". DB_PREFIX ."blocks where type='posts_block' ORDER BY id");
    }

    public function getPosts($id,$offset = 0,$limit = 10){
        return $this->db->query("select * from ". DB_PREFIX ."post where id_post_block=:id ORDER BY id",[":id" => $id]);
    }

    public function getPost($id){
        return $this->db->query("select * from ". DB_PREFIX ."post where id=:id",[":id" => $id])[0];
    }

    public function getBlock($id){
        return $this->db->query("select * from ". DB_PREFIX ."blocks where id=:id",[":id" => $id]);
    }

    public function createPost($items){
        $param = [];
        foreach ($items as $key => $value){
            $param[":".$key] = $value;
        }
        return $this->db->query("INSERT INTO ". DB_PREFIX ."post (id_post_block, title, link, preview_text,full_text,seo_url,meta_title,meta_description,priority,tags,create_date) 
        VALUES(:id_block, :title, :link, :preview_text, :full_text, :seo_url, :meta_title, :meta_description, :priority, :tags, CURRENT_TIMESTAMP);" ,$param);
    }

    public function updatePost($items){
        $param = [];
        foreach ($items as $key => $value){
            $param[":".$key] = $value;
        }
        return $this->db->query("UPDATE ". DB_PREFIX ."post SET title=:title, link=:link, preview_text=:preview_text,full_text=:full_text,seo_url=:seo_url,meta_title=:meta_title,meta_description=:meta_description,priority=:priority,tags=:tags WHERE id=:id",$param);
    }

    public function deletePost($id){
        $this->db->query("delete from ". DB_PREFIX ."post where id=:id",[":id" => $id]);
    }

    public function deleteBlock($id){
        $this->db->query("delete from ". DB_PREFIX ."blocks where id=:id",[":id" => $id]);
        $this->db->query("delete from ". DB_PREFIX ."post where id_post_block=:id",[":id" => $id]);
        $this->db->query("DELETE FROM ". DB_PREFIX ."page_block WHERE id_block=:id ", [":id" => $id]);
    }

}