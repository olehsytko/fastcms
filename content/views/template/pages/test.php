<div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                
                <?php foreach($postBlock as $post):?>
                <div class="post-preview">
                    <a href="<?php echo '/'.$page['seo_url'].'/'.$post['seo_url'] ?>">
                        <h2 class="post-title">
                            <?=$post["title"] ?>
                        </h2>
                        <h3 class="post-subtitle">
                            <?=$post["preview_text"] ?>
                        </h3>
                    </a>
                    <p class="post-meta">Posted by <a href="#">Start Bootstrap</a> on September 24, 2014</p>
                </div>
                <hr>
            <?php endforeach; ?>

               END
                <!-- Pager -->
                <ul class="pager">
                    <li class="next">
                        <a href="#">Older Posts &rarr;</a>
                    </li>
                </ul>
                 <?php foreach ($pagination->buttons as $button): ?>
           <?php if ($button->isActive): ?>
               <?php if($button->text == 1): ?>
                   <?php $url = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH); ?>
                    <a href = '<?=$url?>'><?=$button->text?></a>
               <?php else: ?>
                    <a href = '?page=<?=$button->page?>'><?=$button->text?></a>
                <?php endif; ?>
            <?php else: ?>
               <span style="color:#555555"><?=$button->text?></span>
            <?php endif; ?>
       <?php endforeach;?>
            </div>
        </div>
    </div>