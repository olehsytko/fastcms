<?php 
$lang_text['title'] = "Панель администратора";
$lang_text['text3'] = "Вы уверены, что хотите удалить ";
$lang_text['text4'] = "? Отменить это действие будет невозможно!";
$lang_text['change'] = "Редактировать";
$lang_text['del'] = "Удалить";
$lang_text['save'] = "Сохранить";
$lang_text['cancel'] = "Отменить";

$lang_text['nav_main'] = 'Главная';
$lang_text['nav_pages'] = 'Страницы';
$lang_text['nav_main-parts'] = 'Основные части';
$lang_text['nav_posts'] = 'Посты';
$lang_text['nav_content'] = 'Контент';
$lang_text['nav_settings'] = 'Настройки';
$lang_text['nav_users'] = 'Пользователи';

$lang_text['wlcome'] = "Добро пожаловать в панель администратора";
$lang_text['main_text1'] = "Наведите на раздел слева для просмотра подробной информации."; 
$lang_text['main_text2'] = "Для быстрого сохранения изменений можно нажимать комбинацию Ctrl+S.";
$lang_text['main_text3'] = "База данных успешно подключена";
$lang_text['main_text8'] = "База данных НЕ подключена";
$lang_text['main_text4'] = "Сайт закрыт от индекса!";
$lang_text['main_text9'] = "Сайт индексируется";
$lang_text['main_text5'] = "Мультиязычность включена!";
$lang_text['main_text10'] = "Мультиязычность выключена";
$lang_text['main_text6'] = "Соединение с файлами css и js";
$lang_text['main_text7'] = "Соблюдайте структуру и название папок и файлов!";

$lang_text['main_text11'] = "В этом разделе находятся все страницы сайта с их содержимым.";
$lang_text['main_text12'] = 'Создание блоков "Вывод постов" и "Вывод слайдера" автоматически создаёт блоки в соответствующих разделах. Блоки можно менять местами, для этого используйте кнопки';
$lang_text['main_text13'] = 'Под каждый язык нужно создавать отдельную страницу. Создать язык можно в разделе "Настройки".'; 
$lang_text['main_text14'] = "Создавайте понятные и логичные названия для блоков, не путайте себя и разработчиков!";
$lang_text['main_text15'] = 'У главной страницы в поле "Alias" должно стоять слово "index".';
$lang_text['main_text16'] = "В этом разделе находится верхняя (header.php) и нижняя (footer.php) часть сайта.";
$lang_text['main_text17'] = "Для верхней части главной страницы используется второй редактор (mainPage-header.php).";
$lang_text['main_text18'] = 'В этом разделе вы можете добавлять любые Посты/новости на страницы. Они будут выводиться в блоке "Посты". Блок "Посты" можно добавить на страницы сайта в любом месте. Для этого перейдите в раздел "Страницы", выберите интересующую вас страницу, затем нажмите редактировать. В списке блоков вы можете найти блок уже созданных записей или добавить новый блок. Для добавления нового блока нажмите "Добавить блок", затем выберите блок "Вывод записей", введите название Посты и нажмите "Сохранить". Запись автоматически будет создана в разделе "Посты", где её можно наполнить или изменить. Пагинация для каждой Посты настраивается отдельно.';
$lang_text['main_text19'] = " - Перед началом поста/новости вставьте слово BEGIN, после поста/новости вставьте слово END, название заменить словом HEADER, путь к изображению заменить словом IMG, аналогично можно использовать FULL_TEXT, PREWIEW_TEXT, LINK, INPUT_1, INPUT_2, INPUT_3.";
$lang_text['main_text20'] ='В этом разделе вы можете редатировать слайдеры. Для создания слайдера перейдите в раздел "Страницы" выберите страницу и добавьте новый блок.';
$lang_text['main_text21'] ="В поле название введите название блока";
$lang_text['main_text22'] ="Включите режим разработчика в редакторе, для этого нажмите на кнопку";
$lang_text['main_text23'] ="и вставьте код слайдера (Код должен содержать структуру слайдера и шаблон слайда)";
$lang_text['main_text24'] =" - Перед началом слайда вставьте слово BEGIN, после слайда вставьте слово END, название заменить словом NAME, путь к изображению заменить словом IMG, аналогично можно использовать TEXT, DATE.";
$lang_text['main_text25'] =' - В списке выбрать "Вывод слайдера".';
$lang_text['main_text26'] ="В этом разделе вы можете:";
$lang_text['main_text27'] ="Подключить базу данных";
$lang_text['main_text28'] ="Вставить на сайт Google Tag Manager";
$lang_text['main_text29'] = "Загрузить favicon";
$lang_text['main_text30'] = "Настроить мультиязычность";
$lang_text['main_text31'] = "Закрыть/открыть сайт от индекса";
$lang_text['main_text32'] = "Настроить редиректы";
$lang_text['main_text33'] = "Указать почту, куда будут приходить письма с сайта (Скопируйте код отправщика писем)";
$lang_text['main_text34'] = "В этом разделе вы можете добавить новые доступы к панели администратора или изменить уже созданные.";
$lang_text['main_text35'] = "Есть возможность создать доступ для редактора (Закрыт доступ к разделам разработки).";
$lang_text['main_text36'] = "В этом разделе вы можете редактировать файлы css и js, которые были залиты в соответствующие папки в корне сайта.";
$lang_text['main_text37'] = "В этом разделе вы можете добавить новый функционал в панель администратора.";

$lang_text['pages_text1'] = "Страницы";
$lang_text['pages_text2'] = "Показать";
$lang_text['pages_namepage'] = "Название страницы";

$lang_text['edit_page_text1'] = "Редактировать страницу";
$lang_text['edit_page_text2'] = "Перейти";
$lang_text['edit_page_text3'] = "Название";
$lang_text['edit_page_text4'] = "Выберите тип блока";
$lang_text['edit_page_text5'] = "Вывод постов";
$lang_text['edit_page_text6'] = "Текстовый блок";
$lang_text['edit_page_text7'] = "Добавить";
$lang_text['edit_page_text8'] = "Название";

$lang_text['parts_text1'] = "Основные части";
$lang_text['parts_text2'] = "У Вас нет прав для просмотра этой страницы!";

$lang_text['edit_note_text1'] = "Редактировать страницу";
$lang_text['edit_note_text2'] = "Редактирование постов";
$lang_text['edit_note_text3'] = "Название";
$lang_text['edit_note_text4'] = "Алиас";
$lang_text['edit_note_text5'] = "Фото превью";
$lang_text['edit_note_text6'] = "Выбрать фото";
$lang_text['edit_note_text7'] = "Текст превью";
$lang_text['edit_note_text8'] = "Полный текст";

$lang_text['edit_rewiew_text1'] = "Редактировать контен";
$lang_text['edit_rewiew_text2'] = "Редактирование контента";
$lang_text['edit_rewiew_text3'] = "Название";
$lang_text['edit_rewiew_text4'] = "Фото превью";
$lang_text['edit_rewiew_text5'] = "Выбрать фото";
$lang_text['edit_rewiew_text6'] = "Имя";
$lang_text['edit_rewiew_text7'] = "Фото";
$lang_text['edit_rewiew_text8'] = "Текст";

$lang_text['settings_text1'] = "Настройки";
$lang_text['settings_text2'] = "Код в &lt;head&gt;";
$lang_text['settings_text3'] = "Код после &lt;body&gt;";
$lang_text['settings_text4'] = "Подключение к базе данных";
$lang_text['settings_text5'] = "Хост";
$lang_text['settings_text6'] = "Логин";
$lang_text['settings_text7'] = "Пароль";
$lang_text['settings_text8'] = "Имя базы";
$lang_text['settings_text9'] = "Загрузить favicon";
$lang_text['settings_text10'] = "Языки сайта";
$lang_text['settings_text11'] = "Мультиязычность";
$lang_text['settings_text12'] = "Название";
$lang_text['settings_text13'] = "Алиас";
$lang_text['settings_text14'] = "Закрыть сайт от роботов";
$lang_text['settings_text15'] = "Отправка писем на почту";
$lang_text['settings_text16'] = "Заголовок";
$lang_text['settings_text17'] = "Откуда";
$lang_text['settings_text18'] = "Куда";
$lang_text['settings_text19'] = "Язык админки";
$lang_text['settings_text20'] = "У Вас нет прав для просмотра этой страницы!";
$lang_text['settings_text21'] = "Язык сайта по умолчанию";

$lang_text['users_text1'] = "Логин";
$lang_text['users_text2'] = "Пароль";
$lang_text['users_text3'] = "Редактор";
$lang_text['users_text4'] = "У Вас нет прав для просмотра этой страницы!";