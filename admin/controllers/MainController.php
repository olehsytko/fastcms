<?php
/**
 * Created by PhpStorm.
 * User: dimple
 * Date: 28.03.19
 * Time: 15:07
 */

namespace Fastcms\Controller;


use Fastcms\App\Controller;

class MainController extends Controller
{

    public function index(){

        $this->render('main');
    }

}