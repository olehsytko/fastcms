<?php

namespace Fastcms\App;

class Router{

	protected $uri;
	protected $controller;
	protected $actionSite;
    protected $actionAdmin;
	protected $params;
    protected $get_params;
    protected $route;
	protected $method_prefix;
	protected $language;
 
	public function __construct($uri,$default = []){
		$this->uri = urldecode(trim($uri,'/'));

		$routes = $default['routes'];
		$this->route = $default['route'];
		$this->method_prefix = isset($routes[$this->route]) ? $routes[$this->route]: '';
		$this->language = $default['language'];
		$this->controller = $default['controller'];
		$this->actionSite = $default['action'];
        $this->actionAdmin = $default['action'];

		$uri_parts = explode('?', $this->uri);

		//Get path like lng/controlle/param/para1/...
		$path = $uri_parts[0];

		if(!empty($uri_parts[1])){
            foreach( explode('&', $uri_parts[1]) as $param) {
                list($key, $value) = explode('=', $param);
                $this->get_params[$key] = $value;
            }
        }
		
		$path_parts = explode('/', $path);

		//Get admin
        if(current($path_parts) == 'admin'){
		    array_shift($path_parts);
        }
		
		if( count($path_parts) ){
			//Get route or language at first element
			if( in_array(strtolower(current($path_parts)), array_keys($routes)) ){

				$this->route = strtolower(current($path_parts));

				$this->method_prefix = isset($routes[$this->route]) ? $routes[$this->route]: '';
				array_shift($path_parts);
			} elseif(in_array(strtolower(current($path_parts)) , Config::get("languages")) ) {
				$this->language = strtolower(current($path_parts));
				array_shift($path_parts);
			}

			//Get controller - next element of array
			if( current($path_parts) ){
				$this->controller = strtolower(current($path_parts));
				array_shift($path_parts);
			}

			//Get action 
			if( current($path_parts) ){
				$this->actionSite= strtolower(current($path_parts));
				$alias = explode("-",$this->actionSite);
                $this->actionAdmin = $alias[0];
				if(count($alias) > 1 ){
				    for($i = 1;$i < count($alias); $i++){
                        $this->actionAdmin .= ucfirst($alias[$i]);
                    }
				}
				array_shift($path_parts);
			}

			//Get params - all the rest
			$this->params = $path_parts;
		}
	}

	public static function redirect($location){
		header("Location:{$location}");
	}

	/*
	** Return request stirng;
	*/
	public function getURI(){
		return $this->uri;
	}

	public function getController(){
		return $this->controller;
	}

	public function getSiteAction(){
		return $this->actionSite;
	}

    public function getAdminAction(){
        return $this->actionAdmin;
    }

	public function getParams(){
		return $this->params;
	}

    public function getParamsGet(){
        return $this->get_params;
    }

	public function getRoute(){
		return $this->route;
	}

	public function getMethodPrefix(){
		return $this->method_prefix;
	}

	public function getLanguage(){
		return $this->language;
	}

}