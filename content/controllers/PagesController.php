<?php
/**
 * Created by PhpStorm.
 * User: dimple
 * Date: 26.03.19
 * Time: 14:35
 */

namespace Fastcms\Site\Controller;


use Fastcms\App\Controller;
use Fastcms\App\Router;
use Fastcms\App\Session;
use Fastcms\Site\App;
use Fastcms\Site\Model\Content;
use Fastcms\Site\Model\Page;
use Fastcms\Site\Model\Post;
use Fastcms\Widgets\PaginationWidget;

class PagesController extends Controller
{

    public $title;
    public $description;

    public function __construct(array $data = array())
    {
        parent::__construct($data);
        $this->title = "";
        $this->description = "";
    }

    public function viewPage($urlPage = "index", $pageSection = "none" ){

        $modelPage = new Page();

        //получение данныз о странице из бд
        $page = $modelPage->getPage($urlPage);

        if(!isset($page[0])){
            $this->viewNotFound();
        }

        $page = $page[0];

        $viewBlocks = "";

        //проверка на главную или дочернию страницу
        if($pageSection != "none"){
            $modelPost = new Post();
            $post = $modelPost->getPost($page['id'],$pageSection);

            if(!isset($post[0])){
                $this->viewNotFound();
            }

            $post = $post[0];

            $this->title = $post["meta_title"];
            $this->description = $post["meta_description"];

            $viewBlocks .= $this->getPostContentView($post,$page);

            $this->render('index',['header' => 'header','footer' => 'footer', "viewBlocks" => $viewBlocks]);
        } else {
            $blocks = $modelPage->getPageBlocks($page['id']);

            $this->title = $page["meta_title"];
            $this->description = $page["meta_description"];

            foreach ($blocks as $block){
                if($block['type'] == "posts_block"){
                    $viewBlocks .= $this->getPostBlockView($block["id_block"],$page);
                }
                if($block['type'] == "content_block"){
                    $viewBlocks .= $this->getContentBlockView($block['id_block']);
                }
            }

            if($urlPage == 'index'){
                $this->render('index',['header' => 'header-mainpage','footer' => 'footer-mainpage', "viewBlocks" => $viewBlocks]);
            } else {
                $this->render('index',['header' => 'header','footer' => 'footer', "viewBlocks" => $viewBlocks]);
            }
        }
    }

    public function viewNotFound(){
        ob_start();
        include VIEWS_PATH."/404.php";
        $viewBlocks = ob_get_clean();
        $this->render('index',['header' => 'header','footer' => 'footer', "viewBlocks" => $viewBlocks]);
    }

    // формирует блок со всеми постами на определенной странице
    public function getPostBlockView($idBlock,&$page){
        $post = new Post();
         //получение даных из бд
        $postView = "";

        $countPosts = $post->getTotalCountPosts($idBlock);
        $currentPage = isset($this->get_params['page']) ? $this->get_params['page'] : 1;
        $pagination = new PaginationWidget(['itemsCount' => $countPosts, 'itemsPerPage' => 5,'currentPage' => $currentPage ]);

        $postBlock = $post->getPostBlock($idBlock,$pagination->offset,$pagination->limit);

        //загрузка шаблона
        ob_start();
        include VIEWS_PATH."/post-block.php";
        $postBlockView = ob_get_clean();

        // замена встравок даными из бд
        if(preg_match("/BEGIN([^&]*)END/",$postBlockView,$matches)){
            $postView = $matches[1];
        }
        $replyPost = "";
        foreach ($postBlock as $post) {
            $reply = str_replace("TITLE", $post['title'], $postView);
            $reply = str_replace("TEXT-PREVIEW", $post['preview_text'], $reply);
            $reply = str_replace("LINK", "/".$page['seo_url'].'/'.$post['seo_url'], $reply);

            $replyPost .= $reply;
        }

        //пагинация
        $paginationView = "";

        foreach ($pagination->buttons as $button){
            if ($button->isActive){
                if($button->text == 1){
                    $url = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
                    $paginationView .= "<a href = '{$url}'>{$button->text}</a>\n";
                } else {
                    $paginationView .= "<a href = '?page={$button->page}'>{$button->text}</a>\n";
                }
            } else{
                $paginationView .= "<span style=\"color:#555555\">{$button->text}</span>\n";
            }
        }
        $replyPost .= $paginationView;

        $postBlockView = preg_replace("/BEGIN([^&]*)END/", $replyPost, $postBlockView);

        return $postBlockView;
    }

    // формирует блок с произвольным контентом
    public function getContentBlockView($id_block){
        $modelContent = new Content();
        $content = $modelContent->getContent($id_block);
        return $content["text"];
    }

    public function getPostContentView(&$post,&$page){
        //загрузка шаблона
        ob_start();
        include VIEWS_PATH."/post-content.php";
        $postContentView = ob_get_clean();

//        // замена встравок даными из бд
//        if(preg_match("/BEGIN([^&]*)END/",$postBlockView,$matches)){
//            $postView = $matches[1];
//        }
        $postContentView = str_replace("TITLE", $post['title'], $postContentView);
        $postContentView = str_replace("TEXT-FULL", $post['full_text'], $postContentView);
        $postContentView = str_replace("TAGS", $post['tags'], $postContentView);
        $postContentView = str_replace("BACK", "/".$page['seo_url'], $postContentView);

        return $postContentView;
    }

    // Заменяет или добавляет мета теги
    public function replaceMetaData(&$view){
        $view = str_replace("<title></title>", "<title>".$this->title."</title>", $view);

        if(strpos($view, "description")){
            $view = str_replace('<meta name="description" content="">', "<meta name=\"description\" content=\"".$this->description."\">", $view);
        } else {
            $view = str_replace("</title>", "</title>\n\t<meta name=\"description\" content=\"".$this->description."\">", $view);
        }

        if(strpos($view, "icon")){
            $view = str_replace('<link href="" rel="icon">', "<link href=\"\" rel=\"icon\">", $view);
        } else {
            $view = str_replace("</head>", "<link href=\"\" rel=\"icon\"></head>", $view);
        }
    }

    //отображает страницу на сайте
    public function render($viewPage, $compact = array()){
        extract($compact);

        $layout_path = VIEWS_PATH.DS.$viewPage.'.php';

        $layout_header = VIEWS_PATH_BASE.DS.$header.'.php';
        $layout_footer = VIEWS_PATH_BASE.DS.$footer.'.php';

        $view = "";

        ob_start();
        include $layout_header ;
        $view .= ob_get_clean();

        $view .= "\n";

        $this->replaceMetaData($view);

        ob_start();
        include $layout_path;
        $view .= ob_get_clean();

        $view .= $viewBlocks;

        //$data['content'] = $content;
        $view .= "\n";

        ob_start();
        include $layout_footer;
        $view .= ob_get_clean();

        echo $view;
        exit();
    }
}