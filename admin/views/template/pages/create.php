
<header>
    <h1>Панель управления</h1>
    <div class="breadcrumbs">
        <a href="/admin">Панель управления</a>
        <a href="/admin/pages">Страницы</a>
        <a href="/admin/pages/create">Создание страницы</a>
    </div>
</header>

<form action="/admin/pages/create" method="POST">
    <input type="text" name="name">
    <input type="submit" value="Create page">
</form>