<?php
/**
 * Created by PhpStorm.
 * User: dimple
 * Date: 28.03.19
 * Time: 16:01
 */

namespace Fastcms\Site\Model;


use Fastcms\App\Model;

class Page extends Model
{
    public function getPage($seo_url){
       return $this->db->query("SELECT * FROM ". DB_PREFIX ."pages where seo_url like :seo_url", [":seo_url" => $seo_url]);
    }

    public function getPageBlocks($id){
        return $this->db->query("SELECT * FROM ". DB_PREFIX ."page_block where id_page=:id order by sort_pos", [":id" => $id]);
    }
}