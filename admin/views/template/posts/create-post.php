
<header>
    <h1>Панель управления</h1>
    <div class="breadcrumbs">
        <a href="/admin">Панель управления</a>
        <a href="/admin/posts">Посты</a>
        <a>Создание поста</a>
    </div>
</header>

<form action="/admin/posts/create-post" method="POST">
    <input type="hidden" name="id_block" value="<?= $id_block?>">
    <label>
        Title
        <input type="text" name="title" >
    </label>
    <br/>
    <br/>
    <label>
        SeoUrl
        <input type="text" name="seo_url">
    </label>
    <br/>
    <br/>
    <label>
        Priority
        <input type="text" name="priority" value="0.6">
    </label>
    <br/>
    <br/>
    <label>
        Meta-title
        <input type="text" name="meta_title" >
    </label>
    <br/>
    <br/>
    <label>
        Meta-description
        <input type="text" name="meta_description" >
    </label>
    <br/>
    <br/>
    <label>
        Image
        <input type="file" name="img">
    </label>
    <br/>
    <br/>
    <label>
        Link
        <input type="text" name="link">
    </label>
    <br/>
    <br/>
    <label>
        Preview-text
        <textarea name="preview_text" id="preview-text">
            &lt;p&gt;Here goes the initial content of the editor.&lt;/p&gt;
        </textarea>
    </label>
    <br/>
    <br/>
    <label>
        Full-text
        <textarea name="full_text" id="full-text">
            &lt;p&gt;Here goes the initial content of the editor.&lt;/p&gt;
        </textarea>
    </label>
    <br/>
    <br/>
    <label>
        Tags
        <input type="text" name="tags">
    </label>
    <br/>
    <br/>
    <input type="submit" value="Add post">
</form>


<script src="/system/widgets/ckeditor5/ckeditor.js"></script>
<!--<script src="https://cdn.ckeditor.com/ckeditor5/12.1.0/classic/ckeditor.js"></script>-->
<script>
    ClassicEditor.create( document.querySelector( '#full-text' ), {
        // ckfinder: {
        //     uploadUrl: '/admin/file/upload-image'
        // },
    } )
        .then( editor => {
        console.log( 'Editor was initialized', editor );
        myEditor = editor;
    } )
        .catch( err => {
            console.error( err.stack );
        } );
    ClassicEditor.create( document.querySelector( '#preview-text' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
