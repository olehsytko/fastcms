
$( document ).ready(function() {

    // получаем кол-во ссылок в навигации
    var navCount = 0;
    $('nav .list a').each(function (i, elem) {
        navCount++;
    });

    // анимируем навигацию при загрузке страницы
    $('nav .list.animate').css('left', '0');
    navAnimate();

    // анимированный показ списка навигации
    function navAnimate() {
        var i = 0;
        var navInterval = setInterval(function() {
            $('nav .list a:nth-child(' + i + ')').css({
                'opacity': '1',
                'transform': 'translateX(0)'
            });
            i++;
            if (i > navCount) clearInterval(navInterval);
        }, 50);
    }
    
    // скрываем/показываем анимацию
    $('.resizer').click(function() {
        $(this).toggleClass('active');
        if($(this).hasClass('active')) {
            $(this).removeClass('close');
            $(this).addClass('open');
            $('.panel').addClass('close');
            $('.panel .close-links').fadeIn();
            $('nav .list').css('left', '-290px');
            $(this).css('margin-left', '-288px');
            $('nav .list a').css({
                'opacity': '0',
                'transform': 'translateX(-10px)'
            });
        } else {
            $('.panel .close-links').fadeOut(100);
            $(this).removeClass('open');
            $(this).addClass('close');
            $('.panel').removeClass('close');
            $('nav .list').css('left', '0');
            $(this).css('margin-left', '0');
            navAnimate();
        }
    });

    // открываем/закрываем select
    $('.selected').click(function() {
        if ($(this).hasClass('active')) {
            $(this).delay(400).removeClass('active');
            $('.selected ~ .list').slideUp();
        } else {
            $(this).addClass('active');
            $('.selected ~ .list').slideDown();
        }
    });
    // закрываем select
    $(document).mouseup(function (e) {
        var div = $('.select');
        if (!div.is(e.target)
            && div.has(e.target).length === 0) {
            $('.select .selected').delay(400).removeClass('active');
            $('.selected ~ .list').slideUp();
        }
    });
    // переключаем select
    $('.select .list span').click(function() {
        $(this).parent().parent().children('.selected').children('span').html($(this).html());
        $(this).parent().parent().children('.selected').trigger('click');
    });

    // временное хранение ссылки, до решения юзера
    var tempLink;

    // обрабатываем удаление
    $('[data-delete]').click(function () {
        tempLink = $(this).attr('href');
        $('.confirm').addClass('active');
        return false;
    });
    $('.confirm button').on('click', function () {
        if ($(this).hasClass('red')) {
            window.location.href = tempLink;
            $('.confirm').removeClass('active');
            delete tempLink;
        } else {
            $('.confirm').removeClass('active');
            delete tempLink;
            return false;
        }
    });


    // анимация редактирования страницы
    $('.card .btn-container a:nth-child(1)').on('click',function(e) {
        tempLink = $(this).attr('href');
        e.preventDefault();
        var card = $(this).parent().parent();
        var position = card.offset();
        $(card).css( {
            'transition': 'all .7s ease',
            'transform': 'translateY(-' + position.top + 'px)',
            'height': $('main').height(),
            'width': '100%',
            'border-radius': '0',
            'box-shadow': 'none',
            'background': '#f4f5f9',
            'z-index': '999',
        });
        $(card).addClass('nohover');
        setTimeout(function() {
            window.location.href = tempLink;
        }, 700);
    });
});