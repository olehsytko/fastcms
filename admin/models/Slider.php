<?php


namespace Fastcms\Model;


use Fastcms\App\Model;

class Slider extends Model
{
    public function getSliders(){
       return $this->db->query("SELECT * FROM ". DB_PREFIX ."blocks WHERE type='slider_block'");
    }

    public function getSlider($id){
        return $this->db->query("SELECT * FROM ". DB_PREFIX ."slider WHERE id_block=:id", [":id" => $id]);
    }

    public function getBlock($id){
        return $this->db->query("SELECT * FROM ". DB_PREFIX ."blocks WHERE id=:id", [":id" => $id])[0];
    }
}