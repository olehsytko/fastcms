<?php
/**
 * Created by PhpStorm.
 * User: dimple
 * Date: 26.03.19
 * Time: 14:35
 */

namespace Fastcms\Controller;


use Fastcms\App\App;
use Fastcms\App\Controller;
use Fastcms\App\Router;
use Fastcms\App\Session;
use Fastcms\Model\Page;
use Fastcms\Widgets\PaginationWidget;

class PagesController extends Controller
{

    public function __construct(array $data = array())
    {
        parent::__construct($data);
    }

    public function index(){
        if(!Session::get('user')){
            $data['close'] = "You haven't got";
            $this->render('pages/pages',["data" => $data]);
        } else {

            $model = new Page();

            $countPages = $model->getTotalCountPages();
            $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;
            $pagination = new PaginationWidget(['itemsCount' => $countPages, 'itemsPerPage' => 6,'currentPage' => $currentPage ]);
            $pages = $model->getPages($pagination->offset,$pagination->limit);
            $this->render('pages/pages',['pages' => $pages, 'p' => $pagination, "title" => "Страницы"]);
        }
    }

    public function create(){
        $model = new Page();
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (!empty($_POST['name'])) {
                $name = $_POST['name'];
                $model->addPage($name, $name);
                Router::redirect("/admin/pages");
            }
        }
        $this->render("pages/create",["title" => "Создание страницы"]);
    }


    public function edit(){

        $model = new Page();

        $allBlocks = App::$db->query("select * from ". DB_PREFIX ."blocks");

        if(!empty($_POST)){
            $param = [];
            foreach ($_POST as $key => $value){
                $param[":".$key] = $value;
            }
            $model->savePage($param);
            Router::redirect("/admin/pages/edit/{$_POST['id']}");
        } elseif(isset($this->params[0])){

            //$id = $_GET['id'];
            $id = $this->params[0];

            $page = $model->getPageById($id);

            $boundBlocks = App::$db->query("select id_block,name,sort_pos,pb.type from ". DB_PREFIX ."page_block as pb inner join ". DB_PREFIX ."blocks as b on (pb.id_block=b.id) where id_page=:id",[":id" => $id]);

            $this->render('pages/edit-page',["page" => $page[0], "allBlocks" => $allBlocks, "boundBlocks" => $boundBlocks, "title" => "Редактироваине страницы"]);
        }
    }

    public function attachBlock(){

        if(!empty($_POST)){
            $block_id = $_POST["block"];
            $page_id = $_POST["id"];
            $sort_pos = $_POST["sort_pos"];

            $typeBlock = App::$db->query("SELECT type FROM ". DB_PREFIX ."blocks where id=:id", [":id" => $block_id])[0];

            $model = new Page();

            $model->addBlock($page_id,$block_id,$sort_pos,$typeBlock['type']);
            Router::redirect("/admin/pages/edit/{$page_id}");
        } else {
            Router::redirect("/admin/");
        }
    }

    public function removeBlock(){


        if(!empty($_POST)){
            $block_id = $_POST["id_block"];
            $page_id = $_POST["id_page"];

            $model = new Page();

            $model->removeBlock($page_id,$block_id);
            Router::redirect("/admin/pages/edit/{$page_id}");
        } else {
            Router::redirect("/admin/");
        }
    }

    public function delete(){
        if(!Session::get('user')){
            $this->data['close'] = "You haven't got";
        }

        $model = new Page();

        if($_SERVER["REQUEST_METHOD"]=="POST"){
            if(!empty($_POST['id'])){
                $id = $_POST['id'];
                $model->deletePage($id);
            }
        } elseif($_SERVER["REQUEST_METHOD"]=="GET"){
            if(!empty($_GET['id'])){
                $id = $_GET['id'];
                $model->deletePage($id);
            }
        }
        Router::redirect("/admin/pages");
    }

}