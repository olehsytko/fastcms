<header>
    <h1>Панель управления</h1>
    <div class="breadcrumbs">
        <a href="/admin">Панель управления</a>
        <a href="/admin/posts">Посты</a>
        <a>Редактирование поста</a>
    </div>
</header>

<form action="/admin/posts/edit" method="POST">
    <input type="hidden" name="id_post" value="<?= $post['id']?>">
    <label>
        Title
        <input type="text" name="title" value="<?= $post['title']?>">
    </label>
    <br/>
    <br/>
    <label>
        SeoUrl
        <input type="text" name="seo_url" value="<?= $post['seo_url']?>">
    </label>
    <br/>
    <br/>
    <label>
        Priority
        <input type="text" name="priority" value="<?= $post['priority']?>">
    </label>
    <br/>
    <br/>
    <label>
        Meta-title
        <input type="text" name="meta_title" value="<?= $post['meta_title']?>">
    </label>
    <br/>
    <br/>
    <label>
        Meta-description
        <input type="text" name="meta_description" value="<?= $post['meta_description']?>">
    </label>
    <br/>
    <br/>
    <label>
        Image
        <input type="file" name="img" value="<?= $post['img']?>">
    </label>
    <br/>
    <br/>
    <label>
        Link
        <input type="text" name="link" value="<?= $post['link']?>">
    </label>
    <br/>
    <br/>
    <label>
        Preview-text
        <textarea name="preview_text" id="preview_text" >
           <?php echo htmlentities($post['preview_text']); ?>
        </textarea>
    </label>
    <br/>
    <br/>
    <label>
        Full-text
        <textarea name="full_text" id="full_text">
            <?php echo  htmlentities($post['full_text']); ?>
        </textarea>
    </label>
    <br/>
    <br/>
    <label>
        Tags
        <input type="text" name="tags" value="<?= $post['tags']?>">
    </label>
    <br/>
    <br/>
    <input type="submit" value="Update">
</form>


<script src="/system/widgets/ckeditor4/ckeditor.js"></script>
<!--<script src="https://cdn.ckeditor.com/ckeditor5/12.1.0/classic/ckeditor.js"></script>-->
<script>
    CKEDITOR.replace( 'full_text',{
        filebrowserBrowseUrl: '/system/widgets/ckfinder/ckfinder.html',
        filebrowserUploadUrl: '/system/widgets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
    });
    CKEDITOR.replace( 'preview_text',{
        filebrowserBrowseUrl: '/system/widgets/ckfinder/ckfinder.html',
        filebrowserUploadUrl: '/system/widgets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
    } );
</script>