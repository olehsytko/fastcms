<?php


namespace Fastcms\Model;


use Fastcms\App\Model;

class Content extends Model
{

    public function getContentBlocks($offset = 0,$limit = 10){
        return $this->db->query("select * from ". DB_PREFIX ."blocks where type='content_block' ORDER BY id");
    }

    public function getContentBlock($id_block){
        return $this->db->query("select c.id as id_content,c.id_block as id_block,inputs,text,name from ". DB_PREFIX ."content as c inner join ". DB_PREFIX ."blocks as b on (c.id_block=b.id) where id_block=:id", [":id" => $id_block]);
    }

    public function addContent($items){
        $this->db->query("INSERT INTO ". DB_PREFIX ."blocks(name,type,date_add) VALUES(:name,:type,CURRENT_TIMESTAMP)", [":name" => $items["name"], ":type" => $items["type"] ]);

        $id_block = $this->db->query("SELECT id FROM ". DB_PREFIX ."blocks where name like :name", [":name" => $items["name"]])[0];

        $this->db->query("INSERT INTO ". DB_PREFIX ."content(id_block,text) VALUES(:id_block,:text)", [":id_block" => $id_block["id"], ":text" => $items["text"]]);
    }

    public function updateContentBlock($items){
        $this->db->query("UPDATE ". DB_PREFIX ."blocks SET name=:name where id=:id_block", [":name" => $items["name"], ":id_block" => $items['id_block']]);

        $this->db->query("UPDATE ". DB_PREFIX ."content SET text=:text WHERE id=:id_content", [":id_content" => $items["id_content"], ":text" => $items["text"]]);
    }

    public function deleteContentBlock($id){
        $this->db->query("DELETE FROM ". DB_PREFIX ."blocks WHERE id=:id", [":id" => $id]);

        $this->db->query("DELETE FROM ". DB_PREFIX ."content WHERE id_block=:id ", [":id" => $id]);

        $this->db->query("DELETE FROM ". DB_PREFIX ."page_block WHERE id_block=:id ", [":id" => $id]);
    }

}