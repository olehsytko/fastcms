
<header>
    <h1>Панель управления</h1>

<div class="breadcrumbs">
    <a href="/admin">Панель управления</a>
    <a href="/admin/pages">Страницы</a>
    <a><?=$page["name"]?></a>
</div>
<h1><?=$page["name"]?> - редактировать</h1>
</header>

<a href="/<?=$page['seo_url']?>" target="_blank">Перейти</a>
<br/>
<br/>

<div class="tabs">
    <input id="tab1" type="radio" name="tabs" checked>
    <label for="tab1" title="Вкладка 1">Редактировать</label>

    <input id="tab2" type="radio" name="tabs">
    <label for="tab2" title="Вкладка 2">Добавить блок</label>

    <input id="tab3" type="radio" name="tabs">
    <label for="tab3" title="Вкладка 3">Добавить блок</label>

    <input id="tab4" type="radio" name="tabs">
    <label for="tab4" title="Вкладка 4">Вкладка 4</label>

    <section id="content-tab1">
        <p>Редактировать</p>
        <form action="/admin/pages/edit" method="POST">
            <input type="hidden" name="id" value="<?= $page['id']?>">
            <label>
                Name
                <input type="text" name="name" value="<?=$page["name"]?>">
            </label>
            <br/>
            <br/>
            <label>
                SeoUrl
                <input type="text" name="alias" value="<?=$page["seo_url"]?>">
            </label>
            <br/>
            <br/>
            <label>
                Priority
                <input type="text" name="priority" value="<?=$page["priority"]?>">
            </label>
            <br/>
            <br/>
            <label>
                Language
                <input type="text" name="language_id" value="<?=$page["language_id"]?>">
            </label>
            <br/>
            <br/>
            <label>
                Title
                <input type="text" name="title" value="<?=$page["meta_title"]?>">
            </label>
            <br/>
            <br/>
            <label>
                Description
                <input type="text" name="description" value="<?=$page["meta_description"]?>">
            </label>
            <br/>
            <br/>
            <input type="submit" value="Update">
        </form>
    </section>
    <section id="content-tab2">
        <p>Добавить блок</p>
        <form action="/admin/pages/attach-block" method="post">
            <input type="hidden" name="id" value="<?= $page['id']?>">
            <select name="block">
                <?php foreach ($allBlocks as $block): ?>
                    <option value="<?= $block['id']?>"><?= $block["name"]?></option>
                <?php endforeach; ?>
                <input type="number" value="0" name="sort_pos">
            </select>
            <input type="submit" value="Add">
        </form>
        <div>
            <table>
                <?php foreach ($boundBlocks as $block): ?>
                    <tr>
                        <td><?=$block['name']?></td>
                        <td><?=$block['sort_pos']?></td>
                        <td>
                            <form action="/admin/pages/remove-block" method="post">
                                <input type="hidden" value="<?= $page['id']?>" name="id_page">
                                <input type="hidden" value="<?=$block['id_block']?>" name="id_block">
                                <input type="submit" value="Delete">
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </section>
    <section id="content-tab3">
        <p>Добавить страницу</p>
    </section>
    <section id="content-tab4">
        <p>
            Здесь размещаете любое содержание....
        </p>
    </section>
</div>
