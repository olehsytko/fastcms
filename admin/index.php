<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

define("DS", DIRECTORY_SEPARATOR);
define("ROOT", dirname(__FILE__));
define("VIEWS_PATH", ROOT.DS.'views'.DS.'template');

//require_once(ROOT.DS."conf".DS."config.php");

require_once '../system/vendor/autoload.php';

use Fastcms\App\App;

session_start();

App::startAdminPanel($_SERVER['REQUEST_URI']);